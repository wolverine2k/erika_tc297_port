#ifndef EECFG_H
#define EECFG_H


#define RTDRUID_CONFIGURATOR_NUMBER 1278



/***************************************************************************
 *
 * Common defines ( CPU 2 )
 *
 **************************************************************************/

    /* TASK definition */
    #define EE_MAX_TASK 0

    /* MUTEX definition */
    #define EE_MAX_RESOURCE 0U

    /* EVENT definition */
    #define TimerEvent 0x1U

    /* ALARM definition */
    #define EE_MAX_ALARM 0U

    /* SCHEDULING TABLE definition */
    #define EE_MAX_SCHEDULETABLE 0U

    /* COUNTER OBJECTS definition */
    #define EE_MAX_COUNTER_OBJECTS (EE_MAX_ALARM + EE_MAX_SCHEDULETABLE)

    /* CPU CLOCK definition */
    #define EE_CPU_CLOCK      200000000U

    /* COUNTER definition */
    #define EE_MAX_COUNTER 0U

    /* APPMODE definition */
    #define EE_MAX_APPMODE 0U

    /* CPUs */
    #define EE_MAX_CPU 3
    #define EE_CURRENTCPU 2

    /* Number of isr 2 */
    /* Remote procedure call requires an additional ISR2 */
    #define EE_MAX_ISR2   1
    #define EE_MAX_ISR_ID 0

#ifndef __DISABLE_EEOPT_DEFINES__


/***************************************************************************
 *
 * User options
 *
 **************************************************************************/
#define EE_BUILD_SINGLE_ELF


/***************************************************************************
 *
 * Automatic options
 *
 **************************************************************************/
#define __RTD_CYGWIN__
#define __MSRP__
#define EE_TRICORE__
#define EE_TC27X__
#define EE_TC27XB__
#define EE_GNU__
#define EE_AS_RPC__
#define __OO_ECC2__
#define __OO_HAS_STARTUPHOOK__
#define __MULTI__
#define __OO_NO_RESOURCES__
#define __ALLOW_NESTED_IRQ__

#endif



/***************************************************************************
 *
 * Remote tasks
 *
 **************************************************************************/
    #define Task1 ((EE_TID)0U + (EE_TID)EE_REMOTE_TID)
    #define Task2 ((EE_TID)1U + (EE_TID)EE_REMOTE_TID)


/***************************************************************************
 *
 * Remote alarms
 *
 **************************************************************************/
    #define AlarmTask1 ((EE_TID)0U + (EE_TID)EE_REMOTE_TID)
    #define AlarmTask2 ((EE_TID)1U + (EE_TID)EE_REMOTE_TID)


/***************************************************************************
 *
 * Remote counters
 *
 **************************************************************************/
    #define system_timer ((EE_TID)0U + (EE_TID)EE_REMOTE_TID)
    #define system_timer_slave ((EE_TID)1U + (EE_TID)EE_REMOTE_TID)


/***************************************************************************
 *
 * Remote OsApplication
 *
 **************************************************************************/


/***************************************************************************
 *
 * Remote Schedule Tables
 *
 **************************************************************************/


/***************************************************************************
 *
 * Spin lock IDs
 *
 **************************************************************************/
    #define EE_MAX_SPINLOCK_USER 0

    #define EE_SPINLOCK_CORE0 2U	 /* master */
    #define EE_SPINLOCK_CORE1 1U	 /* slave1 */
    #define EE_SPINLOCK_CORE2 0U	 /* non_os */
    #define EE_MAX_SPINLOCK 3
    #define EE_MAX_SPINLOCK_OS 3


    /* System stack size */
    #define EE_SYS_STACK_SIZE     256



/***************************************************************************
 *
 * Vector size defines
 *
 **************************************************************************/
    #define EE_AS_RPC_ALARMS_SIZE 2
    #define EE_AS_RPC_COUNTERS_SIZE 2
    #define EE_AS_RPC_SERVICES_TABLE_SIZE 5
    #define EE_AS_RPC_TASKS_SIZE 2
    #define EE_RQ_PAIRS_NEXT_SIZE 0
    #define EE_RQ_PAIRS_TID_SIZE 0
    #define EE_RQ_QUEUES_HEAD_SIZE 16
    #define EE_RQ_QUEUES_TAIL_SIZE 16
    #define EE_TC_SYSTEM_TOS_SIZE 1


#endif

