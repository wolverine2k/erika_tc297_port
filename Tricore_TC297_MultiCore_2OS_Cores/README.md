# Tricore TC297 multi-core example with 2 OS cores and 1 non-OS core 

This example shows how Erika Enterprise can be used in multicore configuration on Tricore TC297B. Example runs 2 cores dedicated to Erika OS and 1 non-OS core, but any ratio of Erika OS to non-OS cores can be configured in similar way. Working of OS cores is demonstrated using onboard LEDs without iLLDs (Infineon Low Level Drivers).

We used latest Erika v2 IDE release (November 6th, 2017) with RT-Druid 2.8.0, and **HighTec Free TriCore™ Entry Tool Chain GNU compiler**. Using Erika v2 means that each core has to run separate copy of Erika OS. More on that on Erika's webpage [Erika v2 vs v3](http://www.erika-enterprise.com/index.php/erika3/v2-vs-v3.html).

**_Be sure to replace global paths in Erika OS source location properties and inside HEX generation command (see "Porting guide" section) with actual paths for your system!_** Otherwise, there should be no problem with compiling this project if Erika IDE has been set up properly. More on setting up Erika IDE on Erika's wiki [Erika: Tutorials and Online Documentation](http://erika.tuxfamily.org/wiki/index.php?title=Main_Page#Tutorials_and_Online_Documentation).