# Tricore TC297B Multicore port

This example is a direct result of steps described in "Porting Guide" section in upper directory README.md file. This example does not use LEDs or some onboard periphery to demonstrate the working of Erika OS cores, so you must implement it yourself or see other examples

We used latest Erika v2 IDE release (November 6th, 2017) with RT-Druid 2.8.0, and **HighTec Free TriCore™ Entry Tool Chain GNU compiler**.

**_Be sure to replace global paths in Erika OS source location properties and inside HEX generation command (see "Porting guide" section) with actual paths for your system!_** Otherwise, there should be no problem with compiling this project if Erika IDE has been set up properly. More on setting up Erika IDE on Erika's wiki [Erika: Tutorials and Online Documentation](http://erika.tuxfamily.org/wiki/index.php?title=Main_Page#Tutorials_and_Online_Documentation).