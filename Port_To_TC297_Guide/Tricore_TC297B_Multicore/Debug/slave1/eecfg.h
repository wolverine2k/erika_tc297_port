#ifndef EECFG_H
#define EECFG_H


#define RTDRUID_CONFIGURATOR_NUMBER 1278



/***************************************************************************
 *
 * Common defines ( CPU 1 )
 *
 **************************************************************************/

    /* TASK definition */
    #define EE_MAX_TASK 1
    #define TaskSlave1 0

    /* MUTEX definition */
    #define EE_MAX_RESOURCE 0U

    /* EVENT definition */
    #define TimerEvent 0x1U

    /* ALARM definition */
    #define EE_MAX_ALARM 1U
    #define AlarmSlave1_500ms 0U

    /* SCHEDULING TABLE definition */
    #define EE_MAX_SCHEDULETABLE 0U

    /* COUNTER OBJECTS definition */
    #define EE_MAX_COUNTER_OBJECTS (EE_MAX_ALARM + EE_MAX_SCHEDULETABLE)

    /* CPU CLOCK definition */
    #define EE_CPU_CLOCK      200000000U

    /* COUNTER definition */
    #define EE_MAX_COUNTER 1U
    #define system_timer_slave1 0U

    /* APPMODE definition */
    #define EE_MAX_APPMODE 1U

    /* CPUs */
    #define EE_MAX_CPU 3
    #define EE_CURRENTCPU 1

    /* Number of isr 2 */
    /* Remote procedure call requires an additional ISR2 */
    #define EE_MAX_ISR2   4
    #define EE_MAX_ISR_ID 3

#ifndef __DISABLE_EEOPT_DEFINES__


/***************************************************************************
 *
 * User options
 *
 **************************************************************************/
#define EE_DEBUG
#define EE_SAVE_TEMP_FILES
#define EE_ICACHE_ENABLED
#define EE_DCACHE_ENABLED
#define EE_BUILD_SINGLE_ELF


/***************************************************************************
 *
 * Automatic options
 *
 **************************************************************************/
#define __RTD_CYGWIN__
#define __MSRP__
#define EE_TRICORE__
#define EE_TC27X__
#define EE_TC27XB__
#define EE_GNU__
#define ENABLE_SYSTEM_TIMER
#define __OO_ORTI_LASTERROR__
#define __OO_ORTI_SERVICETRACE__
#define __OO_ORTI_PRIORITY__
#define __OO_ORTI_RES_LOCKER_TASK__
#define __OO_ORTI_RES_ISLOCKED__
#define __OO_ORTI_STACK__
#define __OO_ORTI_ALARMTIME__
#define __OO_ORTI_RUNNINGISR2__
#define EE_AS_RPC__
#define __OO_ECC1__
#define __OO_EXTENDED_STATUS__
#define __OO_HAS_ERRORHOOK__
#define __MULTI__
#define __OO_NO_RESOURCES__
#define __OO_AUTOSTART_TASK__
#define __OO_AUTOSTART_ALARM__
#define __ALLOW_NESTED_IRQ__

#endif



/***************************************************************************
 *
 * Remote tasks
 *
 **************************************************************************/
    #define TaskMaster ((EE_TID)0U + (EE_TID)EE_REMOTE_TID)
    #define TaskSlave2 ((EE_TID)2U + (EE_TID)EE_REMOTE_TID)


/***************************************************************************
 *
 * Remote alarms
 *
 **************************************************************************/
    #define AlarmMaster_1s ((EE_TID)0U + (EE_TID)EE_REMOTE_TID)
    #define AlarmSlave2 ((EE_TID)2U + (EE_TID)EE_REMOTE_TID)


/***************************************************************************
 *
 * Remote counters
 *
 **************************************************************************/
    #define system_timer_master ((EE_TID)0U + (EE_TID)EE_REMOTE_TID)
    #define system_timer_slave2 ((EE_TID)2U + (EE_TID)EE_REMOTE_TID)


/***************************************************************************
 *
 * Remote OsApplication
 *
 **************************************************************************/


/***************************************************************************
 *
 * Remote Schedule Tables
 *
 **************************************************************************/


/***************************************************************************
 *
 * Spin lock IDs
 *
 **************************************************************************/
    #define EE_MAX_SPINLOCK_USER 0

    #define EE_SPINLOCK_CORE0 2U	 /* master */
    #define EE_SPINLOCK_CORE1 1U	 /* slave1 */
    #define EE_SPINLOCK_CORE2 0U	 /* slave2 */
    #define EE_MAX_SPINLOCK 3
    #define EE_MAX_SPINLOCK_OS 3


    /* System stack size */
    #define EE_SYS_STACK_SIZE     256



/***************************************************************************
 *
 * HW counter definition
 *
 **************************************************************************/
#define EE_MAX_COUNTER_HW 1
#define EE_SYSTEM_TIMER   system_timer_slave1
#define EE_SYSTEM_TIMER_DEVICE   EE_TC_STM_SR0

#define EE_TC_2_ISR EE_tc_system_timer_handler
#define EE_TC_2_ISR_PRI EE_ISR_PRI_2
#define EE_TC_2_ISR_CAT 2
#define EE_ISR2_ID_EE_tc_system_timer_handler 2

/* Max ISR priority */
#define EE_TC_MAX_ISR_ID     EE_ISR_PRI_2



/***************************************************************************
 *
 * Counter defines
 *
 **************************************************************************/
#define OSMAXALLOWEDVALUE_system_timer_slave1 2147483647U
#define OSTICKSPERBASE_system_timer_slave1    1U
#define OSMINCYCLE_system_timer_slave1        1U

#define OSMAXALLOWEDVALUE 2147483647U
#define OSTICKSPERBASE    1U
#define OSMINCYCLE        1U
#define OSTICKDURATION    10000000U



/***************************************************************************
 *
 * Vector size defines
 *
 **************************************************************************/
    #define EE_ACTION_ROM_SIZE 1
    #define EE_ALARM_ROM_SIZE 1
    #define EE_AS_RPC_ALARMS_SIZE 3
    #define EE_AS_RPC_COUNTERS_SIZE 3
    #define EE_AS_RPC_SERVICES_TABLE_SIZE 5
    #define EE_AS_RPC_TASKS_SIZE 3
    #define EE_COUNTER_HW_ROM_SIZE 1
    #define EE_COUNTER_OBJECTS_ROM_SIZE 1
    #define EE_COUNTER_ROM_SIZE 1
    #define EE_OO_AUTOSTART_ALARM_MODE_OSDEFAULTAPPMODE_SIZE 1
    #define EE_OO_AUTOSTART_TASK_MODE_OSDEFAULTAPPMODE_SIZE 1
    #define EE_TC_SYSTEM_TOS_SIZE 2


#endif

