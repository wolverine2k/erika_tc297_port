# Tricore_TC297B_Multicore

Tricore_TC297B_Multicore directory contains TC297B multicore example for Erika IDE which is a direct result of instructions described below in “**Porting guide**” section. This example does not use LEDs or some onboard periphery to demonstrate the working of Erika OS cores, so you must implement it yourself or see other examples.


# Porting guide
This section describes how to modify Erika's default multicore example for TC275 inside **Erika IDE** to run on a **TC297 TFT B-step** board or TC2xx in general. Instructions on making singlecore TC2xx example can be derived from this instructions. 

We used latest Erika v2 IDE release (November 6th, 2017) with RT-Druid 2.8.0, and **HighTec Free TriCore™ Entry Tool Chain GNU compiler**. Our motivation to use Erika IDE instead of Infineon's BiFACES IDE was because we came to the conclusion that BiFACES's Tricore 2xx single core example with Erika OS cannot be easily extended to multicore.

## Creating default multicore project
1. File → New → RT-Druid Oil and C/C++ Project
2. Insert project name
3. Click “_Next >_” button
4. Check “_Create a project using one of the templates_”
5. Select “_tricore/infineon_TriBoard-TC2X5_V2.0/Multicore examples/Multicore Event demo_” project

## Modifying default multicore project

1. We will need to modify some Erika OS files, so we copy OS folder inside our project:
	1. Your project → Properties → Erika Files Location
	2. Check “_Enable project specific settings_”
	3. In “_Auto_” Erika source location option there is a path to original OS folder - “_... \EE_RT-Druid-2.8.0-juno-win32\eclipse\plugins\com.eu.evidence.ee_2.8.0.20171025_0922\ee_files_”.
	4. Copy original “_ee_files_” folder to your project (only “_pkg_” sub-directory is needed, but some non-Tricore files and folders can be safely removed inside corresponding sub-directories “_pkg/board_”, “_pkg/cpu_”, “_pkg/mcu_”, “_pkg/cfg/arch_”)
	5. In Erika source location settings select “_Manual_” source location and insert path to your copy of Erika OS (path must have pkg folder as sub-directory)

	![Alt](../img/Project_properties.png "Erika source location properties")

2. Now we need to modify 3 OS files:
	* In file “_pkg/cpu/tricore/inc/_” replace two lines:
		* `#include <tc27xb/Ifx_reg.h>` → `#include <tc29xb/Ifx_reg.h>` (Because I will later define TC27xB model in OIL file, but you may as well comment all similar lines and leave only your model specific code line)
		* `#define  EE_INCLUDE_MCU_REGS <sfr/TC27x/Ifx_reg.h>` → `#define  EE_INCLUDE_MCU_REGS <sfr/TC29x/Ifx_reg.h>`
	* In file “_pkg/cfg/arch/cc_tricore_gnu_new.mk_” replace `GNUC_TRICORE_MODEL := tc27xx` →  `GNUC_TRICORE_MODEL := tc29xx` (flag for HighTec GNU compiler)
	* If you want to generate hex file from elf files, then you need to place the following code line `"C:/HighTec/toolchains/tricore/v4.9.1.0-infineon-1.1/bin/tricore-objcopy.exe" tc27x_multicore.elf -O ihex tc27x_multicore.hex` in “_pkg/cfg/arch/rules_infineon_multi_base.mk_” just before `@echo "Global Compilation terminated successfully!"` command in order for it to execute in the end of the compilation.
	**_Hex generation command line may be different depending on HighTec toolchain installation folder and name of the generated elf file, which differs for singlecore configuration!_**

3. Configure OS by modifying OIL file (conf.oil):
	* `EE_OPT = "EE_EXECUTE_FROM_RAM";` option must be commented, because otherwise the code will be located in different memory section, other options can be left untouched in order for the project to compile. 
	* Add option `EE_OPT = "EE_BUILD_SINGLE_ELF";` in order to generate a single elf file, which is merged from the elf files generated for each core. Its default name is “_tc27x_multicore.elf_”.
	* Change `MODEL = TC27x;` → `MODEL = TC27xB;` where B stands for B-step (TC27x does not compiles for me)
	* `BOARD_DATA = TRIBOARD_TC2X5;` must be commented, because it includes TC275 board specific functions which we can not use.
	* The rest of OIL commands depend on application and can be changed according to your needs. In this example we try to leave as much of the original code as possible and compile the project, without adding TC297 board dependent functions. So next steps can be omitted if you already know how to create Erika OS application:
		* Remove `EVENT = ButtonEvent;`, `EVENT ButtonEvent { MASK = AUTO; };` and `ISR Button_isr2{...}` because this example sets interrupt request on button using board dependent function.

4. Steps below also can be omitted if you know how to create Erika OS application. Remove board specific functions from source files:
	* In “_shared.h_” all code can be removed except for includes.
	* In “_master.c_” all “led_blink” functions should be commented or replaced if necessary, also `EE_tc2x5_leds_init` needs to be removed from main function.
	* In “_slave1.c_”:
		* In “_slave1_toogle_led_” function everything should be removed from the inside, but the function will be triggered by an alarm, so it may be useful later. 
		* In `TASK(TaskSlave1)` function remove button event:
			* `WaitEvent(TimerEvent | ButtonEvent);` → `WaitEvent(TimerEvent);`
			* Remove `if (mask & ButtonEvent) {...}` part
			* Remove `led_blink` from `if (mask & TimerEvent) {...}` part
	* In “_slave2.c_” remove `EE_tc2x5_button_irq_init`, `led_blink` and button IRQ handler.

## Building the project
Build the project, it should compile without errors if all steps were made. Compilation may take some time, because each core needs to be compiled separately, but **_parallel compilation in Erika IDE does not work properly for multicore_**. 
