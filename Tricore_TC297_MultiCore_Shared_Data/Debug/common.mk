

############################################################################
#
# Makefile created by RT-Druid
# 
# Common file
#
############################################################################

# Erika base directory
ifneq ($(__ERIKA_INNER_MAKEFILE_CALL__), yes)
ifdef ERIKA_FILES
ifdef EEBASE
    $(warning EEBASE is set, but it has been overridden by ERIKA_FILES)
endif
EEBASE := $(shell cygpath `cygpath -ms '${ERIKA_FILES}'`)

else # ERIKA_FILES

ifndef EEBASE
        EEBASE := $(shell cygpath `cygpath -ms 'C:\Users\User\ERIKAworkspace\Tricore_TC297_MultiCore_2OS_Cores_Shared\OS'`)
else
        $(warning The usage of EEBASE is deprecated. Please use ERIKA_FILES)
endif
endif # ERIKA_FILES
# ERIKA_FILES has fulfilled its role. Make sure it's not used inside Erika makefiles
ERIKA_FILES :=

$(info Using erika files in $(EEBASE))
endif # __ERIKA_INNER_MAKEFILE_CALL__


RTDRUID_CONFIGURATOR_NUMBER:=1278



############################################################################
#
# Common EE options
#
############################################################################
EEOPT += 
EEOPT += EE_BUILD_SINGLE_ELF
EEOPT += __RTD_CYGWIN__
EEOPT += __MSRP__
EEOPT += EE_TRICORE__
EEOPT += EE_TC27X__
EEOPT += EE_TC27XB__
EEOPT += EE_AS_RPC__
EEOPT += __OO_ECC2__
EEOPT += __OO_HAS_STARTUPHOOK__
EEOPT += __MULTI__
EEOPT += __DISABLE_EEOPT_DEFINES__



############################################################################
#
# Flags
#
############################################################################
CFLAGS   = -O2

ASFLAGS  = 

LDFLAGS := 

LDDEPS  += 

LIBS    := 

TRICORE_MODEL  := tc27x

-include $(EEBASE)/pkg/cfg/path_helper.mk
$(eval $(call check_and_set_cygwin_compiler_path,TRICORE_GCCDIR,C:\HighTec\toolchains\tricore\v4.9.1.0-infineon-1.1))
