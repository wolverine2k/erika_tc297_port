/*
 * Main Erika OS Core - Master core
 */
#include "shared.h"	// Also includes all Tricore related libraries

volatile int EE_SHARED_IDATA led_status = 0;	// shared data

DeclareTask(Task1);
DeclareEvent(TimerEvent);

// We use resources to protect shared data
static void led_blink()
{
	static Ifx_P * const portLEDS = (Ifx_P *)&MODULE_P13;
	GetResource(ResourcePORT);
	portLEDS->OUT.U = (led_status % 16);		// Display value on 4 LEDs | Not the safest way to change LED values
	ReleaseResource(ResourcePORT);
}

// Extended task that waits for alarm event (Never terminates)
TASK(Task1)
{
	EventMaskType mask;
	while (1)
	{
		WaitEvent(TimerEvent);
		GetEvent(Task1, &mask);

		if (mask & TimerEvent)
		{
			led_blink();
			ClearEvent(TimerEvent);
		}
	}
	TerminateTask();	// Never executes
}

// Could be used for initialisation.
void StartupHook(void){}

// MAIN
int main( void )
{
	Init_LEDs();	// Initialise LEDs here, because I am not sure that other cores
					// won't use LEDs before Erika's StartupHook is executed

	StatusType status;
	StartCore(OS_CORE_ID_1, &status);	// This is responsible for launching Erika on 2nd CPU (I think they go by the order in OIL)
	StartNonAutosarCore(OS_CORE_ID_2, &status); // This one is not OS core, although we define it in OIL because of OSEK reasons
	// StartCore won't work instead of StartNonAutosarCore !

	StartOS(OSDEFAULTAPPMODE);	// Now we launch Erika OS on this core

  	return 0;
}

void Init_LEDs()
{
	// Define GPIO port
	static Ifx_P * const portLEDS = (Ifx_P *)&MODULE_P13; // Only module 13 four LEDs can be used
	portLEDS->IOCR0.U = 0x80808080;	// Inits 0-3 pins as push-pull general purpose output pins
	portLEDS->OUT.U |= 0xf; // Turn off all 4 LEDs
}
