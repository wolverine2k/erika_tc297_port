/*
 * NO OS Core
 */

// Libs (also iLLD) can be also put inside CPU section of OIL file, for specific section,
// however it will result in recompilation of OIL file and all cores, while inclusion
// of libraries in each core specific source file only will cause this core to recompile
// (I can also include ee.h, because it contains reference to Hightec Tricore files,
// but I don't want to include ee.h in non OS core)
#include "inc/tc29xb/IfxPort_bf.h"
#include "inc/tc29xb/IfxPort_reg.h"
#include "inc/tc29xb/IfxPort_regdef.h"

int main(void)
{
	static Ifx_P * const portLEDS = (Ifx_P *)&MODULE_P13;
	//portLEDS->OUT.B.P3 = 0;		// Turn 4th LED ON . There are two reasons why interrupt blocking is not used here:
									// 1. This code runs before cores with Erika will be running OS
									// 2. We need to include interrupt functions, but I am not sure where they are located



	// Just loop forever
	while(1)
	{

	}
	return 0;
}
