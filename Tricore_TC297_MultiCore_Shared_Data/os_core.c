/*
 * Second Erika OS Core - Slave core
 */
#include "shared.h"	// Also includes all Tricore related libraries

extern volatile int EE_SHARED_IDATA led_status;	// shared data

DeclareTask(Task2);

// Basic or one shot task, which must terminate after every activation
TASK(Task2)
{
	static Ifx_P * const portLEDS = (Ifx_P *)&MODULE_P13;

	// Resources for shared data protection
	GetResource(ResourcePORT);
	led_status++;
	ReleaseResource(ResourcePORT);

	TerminateTask();
}


// MAIN TASK
int main(void)
{
  StartOS(DONOTCARE);	// Usually CPU specific APPMODE goes as a parameter, we did not use them in OIL
  return 0;
}
