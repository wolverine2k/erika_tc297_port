# Motivation


There is no official Erika Enterprise OS port available for Aurix TC29x-series. For this reason EDI (Latvian Institute of Electronics and Computer Science) during 3Ccar project (Horizon 2020) has developed Erika OS port for TC297, but same instructions can be used to port Erika to any TC2xx microcontroller. 

# Information

We based our TC297 port on available Erika's official TC27x multicore port, and used latest official Erika IDE release (November 6th, 2017) with RT_Druid 2.8.0 OSEK configuration tool and latest Erika v2 OS version, because Erika v3 has no documentation regarding AURIX family available yet.

_This work is result of activities within the 3Ccar project, which has received funding from ECSEL Joint Undertaking under grant agreement No. 662192. This Joint Undertaking received support from the European Unions Horizon 2020 research and innovation programme and Germany, Austria, Czech Republic, Romania, Belgium, United Kingdom, France, Netherlands, Latvia, Finland, Spain, Italy, Lithuania._

# Limitations

* As our port is not official, it is also not OSEK/VDX certified, but it shows that Erika OS can be ported to any AURIX TC2xx-series microcontroller.
* This port does not modify RT_Druid definitions or plugins to support TC297 mcu, as a workaround we modify OS files directly to treat “TC27x” symbol as “TC29x”.

# License

We provide this examples and instructions in the hope that they will be useful, but **WITHOUT ANY WARRANTY**. There are no restrictions regarding usage of examples provided in this project as long as it doesn't contradict with original Erika Enterprise license, i.e. GPL2+Linking exception for the kernel.


# Project structure

## Port_To_TC297_Guide
This directory contains “Porting guide” section that describes how to modify Erika's default multicore example for TC275 inside Erika IDE to run on a TC297 TFT B-step board or TC2xx in general.

### Tricore_TC297B_Multicore 
This directory contains TC297B multicore example for Erika IDE which is a direct result of instructions described in “Porting guide” section. This example uses 3 OS cores, and does not use LEDs or some onboard periphery to demonstrate the working of Erika OS cores, so you must implement it yourself or see other examples.

## Tricore_TC297_MultiCore_2OS_Cores  
This example demonstrates how Erika Enterprise can be used in multicore configuration on Tricore TC297B. Example runs 2 cores dedicated to Erika OS and 1 non-OS core, but any ratio of Erika OS to non-OS cores can be configured in similar way. Working of OS cores is demonstrated using onboard LEDs without iLLDs (Infineon Low Level Drivers).

## Tricore_TC297_MultiCore_Shared_Data
This example demonstrates how variable can be shared across two OS cores. Second OS core increments shared integer variable each second while first OS core displays its value on LEDs at 2 Hz frequency, although Event can be generated between cores using `USEREMOTEEVENT = ALWAYS;` option in OIL file. Also `USEREMOTETASK = ALWAYS;` is enabled and used in this example by allowing Alarm on first core to trigger task on 2nd, and otherwise for Alarm on second core.

More on shared data in Erika and why “export_master.exp” file is needed at [Erika forum: shared data](http://www.erika-enterprise.com/forum/viewtopic.php?f=4&t=1071) and [Erika wiki: multicore support](http://erika.tuxfamily.org/wiki/index.php?title=ERIKA_multicore_support).


## Tricore_TC297_MultiCore_iLLD
This example demonstrates how Erika OS can be used with Infineon Low Level Drivers (iLLDs) inside Erika IDE. 2 OS cores blink LEDs without iLLDs while 1 non-OS core uses iLLDs. Other modules can be controlled similarly using according iLLDs. Example does not contain folder with iLLDs, because it may be against Infineon's license, so you must copy them manually (iLLD version 1.0.0.8.0). Driver integration was done with the help of Erika forum: [Erika forum](http://www.erika-enterprise.com/forum/viewtopic.php?t=1103).


## img
This folder contains images used in README files across this project.
