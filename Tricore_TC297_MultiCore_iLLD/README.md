# Tricore TC297 multi-core example with iLLD drivers

This example demonstrates how Erika OS can be used with Infineon Low Level Drivers (iLLDs) inside Erika IDE. 2 OS cores blink LEDs without iLLDs while 1 non-OS core uses iLLDs. Other modules can be controlled similarly using according iLLDs. Example does not contain folder with iLLDs, because it may be against Infineon's license, so you must copy them manually (iLLD version 1.0.0.8.0) inside directory in corresponding directories: 
* 0_AppSw to include/AppSw;
* 1_SrvSw to include/SrvSw;
* 4_McHal to include/McHal;

Driver integration was done with the help of Erika forum: [Erika forum](http://www.erika-enterprise.com/forum/viewtopic.php?t=1103).

We used latest Erika v2 IDE release (November 6th, 2017) with RT-Druid 2.8.0, and **HighTec Free TriCore™ Entry Tool Chain GNU compiler**. Using Erika v2 means that each core has to run separate copy of Erika OS. More on that on Erika's webpage [Erika v2 vs v3](http://www.erika-enterprise.com/index.php/erika3/v2-vs-v3.html).

**_Be sure to replace global paths in Erika OS source location properties and inside HEX generation command (see "Porting guide" section) with actual paths for your system!_** Otherwise, there should be no problem with compiling this project if Erika IDE has been set up properly. More on setting up Erika IDE on Erika's wiki [Erika: Tutorials and Online Documentation](http://erika.tuxfamily.org/wiki/index.php?title=Main_Page#Tutorials_and_Online_Documentation).