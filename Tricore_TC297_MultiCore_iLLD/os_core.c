/*
 * Second Erika OS Core - Slave core
 */
#include "ee.h"	// Also includes all Tricore related libraries
#include "ee_irq.h"

DeclareTask(Task2);

// Basic or one shot task, which must terminate after every activation
TASK(Task2)
{
	static Ifx_P * const portLEDS = (Ifx_P *)&MODULE_P13;

	DisableAllInterrupts();
	portLEDS->OUT.B.P1 ^= 1;		// Toggle 3rd LED
	EnableAllInterrupts();

	TerminateTask();
}


// MAIN TASK
int main(void)
{
  StartOS(DONOTCARE);	// Usually CPU specific APPMODE goes as a parameter, we did not use them in OIL
  return 0;
}
