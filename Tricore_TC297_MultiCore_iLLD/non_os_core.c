/*
 * NO OS Core
 */

// Libs (also iLLD) can be also put inside CPU section of OIL file, for specific section,
// however it will result in recompilation of OIL file and all cores, while inclusion
// of libraries in each core specific source file only will cause this core to recompile
// (I can also include ee.h, because it contains reference to Hightec Tricore files,
// but I don't want to include ee.h in non OS core)
#include "inc/tc29xb/IfxPort_bf.h"
#include "inc/tc29xb/IfxPort_reg.h"
#include "inc/tc29xb/IfxPort_regdef.h"

// iLLD
#include "include/McHal/Tricore/Port/Std/IfxPort.h"
#include "include/McHal/Tricore/_Reg/IfxStm_reg.h"
#include "include/McHal/Tricore/Stm/Std/IfxStm.h"

int main(void)
{
	static Ifx_P * const portLEDS = (Ifx_P *)&MODULE_P13;

	// LEDs have been initialized up to this point
	IfxPort_setPinHigh(&MODULE_P13, 3);
	IfxPort_setPinLow(&MODULE_P13, 2);

	// Just loop forever
	while(1)
	{

	}
	return 0;
}
